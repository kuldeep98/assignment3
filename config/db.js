import mongoose from 'mongoose'

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.MONGOURI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    })
    console.log('Database connected....!!')
  } catch (error) {
    console.log('Error while connecting to Database.....')
  }
}

export default connectDB
