const getPrice = (availableGas) => {
  const gasPrice = { petrol: 100, diesel: 78, cng: 50 }
  return availableGas.map((gas) => {
    let price
    let gasType = gas.toLowerCase()
    if (/petrol/i.test(gasType)) price = gasPrice.petrol
    else if (/cng/i.test(gasType)) price = gasPrice.cng
    else if (/diesel/i.test(gasType)) price = gasPrice.diesel
    return { gasType: gasType, price: price }
  })
}

export { getPrice }
