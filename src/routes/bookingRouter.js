import { Router } from 'express'
import {
  createBooking,
  getAllBooking,
  getBookingByStation,
  getBookingByUser,
} from '../controllers/bookingController.js'
import auth from '../middlewares/auth.js'
const router = Router()

router.post('/create', auth, createBooking)
router.get('/', getAllBooking)
router.get('/user', auth, getBookingByUser)
router.get('/station/:id', getBookingByStation)

export default router
