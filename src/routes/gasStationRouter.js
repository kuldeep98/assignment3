import { Router } from 'express'
import {
  createGasStation,
  getAllGasStation,
  getGasStationBySector,
  getNearestGasStation,
} from '../controllers/gasStationController.js'
const router = Router()

router.post('/create', createGasStation)
router.get('/', getAllGasStation)
router.get('/bysector', getGasStationBySector)
router.get('/near', getNearestGasStation)

export default router
