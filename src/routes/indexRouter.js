import { Router } from 'express'
import userRouter from './userRouter.js'
import gasStationRouter from './gasStationRouter.js'
import bookingRouter from './bookingRouter.js'
const router = Router()

router.use('/users', userRouter)
router.use('/station', gasStationRouter)
router.use('/booking', bookingRouter)
export default router
