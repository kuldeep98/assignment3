import { Router } from 'express'
import {
  blockUser,
  getAllUsers,
  loginUser,
  registerUser,
} from '../controllers/userController.js'

const router = Router()

router.post('/register', registerUser)
router.post('/login', loginUser)
router.post('/status', blockUser)
router.get('/', getAllUsers)
export default router
