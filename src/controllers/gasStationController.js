import GasStation from '../models/gasStationModel.js'
import { getPrice } from '../utils/getGasPrice.js'

const createGasStation = async (req, res) => {
  try {
    const { name, city, sector, availableGas, phone } = req.body
    const gas = getPrice(availableGas)

    const address = { city: city.toLowerCase(), sector }
    const gasStation = await GasStation.create({
      name,
      phone,
      availableGas: gas,
      address,
    })
    res.status(200).json({ gasStation })
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

const getAllGasStation = async (req, res) => {
  try {
    const gasStations = await GasStation.find()
    if (!gasStations)
      return res.status(404).json({ err: 'No gas station registered yet' })
    res.status(200).json(gasStations)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

const getGasStationBySector = async (req, res) => {
  try {
    const { sector, city } = req.body
    console.log(sector)
    const gasStations = await GasStation.find()
      .where('address.city')
      .equals(city)
      .where('address.sector')
      .equals(sector)
    if (!gasStations)
      return res
        .status(404)
        .json({ err: `No Station found at sector ${sector}` })
    res.status(200).json(gasStations)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

const getNearestGasStation = async (req, res) => {
  try {
    const { sector } = req.body
    const gasStations = await GasStation.find()
      .where('address.sector')
      .lte(sector + 5)
      .gte(sector - 5)
    res.json(gasStations)
  } catch (error) {
    res.status(500).json({ err: err.message })
  }
}

export {
  createGasStation,
  getAllGasStation,
  getGasStationBySector,
  getNearestGasStation,
}
