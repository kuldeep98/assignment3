import Booking from '../models/bookingModel.js'

const createBooking = async (req, res) => {
  try {
    const userId = req.user.id
    if (!userId)
      return res.status(401).json({ err: 'Kindly Login to create booking' })
    const { gasStationId, amountPaid } = req.body
    const booking = await Booking.create({
      user: userId,
      gasStation: gasStationId,
      amountPaid,
    })
    res.status(200).json(booking)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}
const getAllBooking = async (req, res) => {
  try {
    const bookings = await Booking.find()
      .populate('user', ['name', 'phone'])
      .populate('gasStation', ['address', 'phone'])
    if (!bookings) return res.status(404).json({ err: 'No Booking Yet..!' })
    res.status(200).json(bookings)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

const getBookingByUser = async (req, res) => {
  try {
    const userId = req.user.id
    const bookings = await Booking.find({ user: userId }).populate(
      'gasStation',
      ['name', 'address', 'phone']
    )
    if (!bookings)
      return res.status(404).json({ err: 'No booking found for this User' })
    res.status(200).json(bookings)
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}

const getBookingByStation = async (req, res) => {
  try {
    const gasStationId = req.params.id
    const bookings = await Booking.find({
      gasStation: gasStationId,
    }).populate('user', ['name', 'phone', 'status', 'vehicleType'])
    if (!bookings) res.status(404).json({ err: 'No Booking Found ' })
    const result = bookings.filter((booking) => booking.user.status === true)
    res.status(200).json(result)
  } catch (err) {
    res.status(500).json({err:err.message})
  }
}
export { createBooking, getAllBooking, getBookingByUser, getBookingByStation }
