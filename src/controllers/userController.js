import User from '../models/userModel.js'
import generateToken from '../utils/generateToken.js'

const registerUser = async (req, res) => {
  try {
    const { name, phone, email, password, city, sector, vehicleType } = req.body
    const isUser = await User.findOne({ email })
    if (isUser) return res.status(400).json({ err: 'User already exists' })
    const address = { sector, city: city.toLowerCase() }
    const user = await User.create({
      name,
      email,
      phone,
      password,
      address,
      vehicleType: vehicleType.toLowerCase(),
    })
    res.status(200).json({ user, token: generateToken(user._id) })
  } catch (err) {
    res.status(500).json(err.message)
  }
}

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body
    const user = await User.findOne({ email })
    if (user && (await user.matchPassword(password))) {
      res.status(200).json({ token: generateToken(user.id) })
    } else {
      res.status(401).json({ err: 'Invalid Credentials' })
    }
  } catch (err) {
    res.status(500).json(err.message)
  }
}

const getAllUsers = async (req, res) => {
  try {
    const users = await User.find().select('-password')
    res.status(200).json(users)
  } catch (error) {
    res.json({ err: error.message })
  }
}

const blockUser = async (req, res) => {
  try {
    const { userId, blockOrUnblock } = req.body
    const user = await User.findOne({ _id: userId })
    user.status = blockOrUnblock
    await user.save()
    res.status(200).json({
      status: user.status === true ? 'Unblocked' : 'Blocked',
      msg: 'User status updated',
    })
  } catch (err) {
    res.status(500).json({ err: err.message })
  }
}
export { registerUser, loginUser, getAllUsers, blockUser }
