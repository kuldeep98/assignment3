import jwt from 'jsonwebtoken'
import User from '../models/userModel.js'

const auth = async (req, res, next) => {
  try {
    const token = req.header('x-auth-token')
    const decoded = jwt.verify(token, process.env.TOKEN_SECRET)
    console.log(decoded)
    const user = await User.findById(decoded.userId).select('-password')
    console.log(user)
    req.user = user
    next()
  } catch (err) {
    res.status(401).json({ err: 'User not Authorized' })
  }
}

export default auth
