import mongoose from 'mongoose'

const gasStationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    availableGas: [
      {
        gasType: {
          type: String,
          required: true,
          enum: ['petrol', 'cng', 'diesel'],
        },
        price: {
          type: Number,
          required: true,
        },
      },
    ],
    address: {
      city: {
        type: String,
        required: true,
      },
      sector: {
        type: Number,
        required: true,
      },
    },
  },
  {
    timestamps: true,
  }
)

const GasStation = mongoose.model('gas_stations', gasStationSchema)

export default GasStation
