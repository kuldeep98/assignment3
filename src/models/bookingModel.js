import mongoose from 'mongoose'

const bookingSchema = new mongoose.Schema(
  {
    amountPaid: {
      type: Number,
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'users',
    },
    gasStation: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'gas_stations',
    },
  },
  {
    timestamps: true,
  }
)

const Booking = mongoose.model('bookings', bookingSchema)

export default Booking
