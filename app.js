import express from 'express'
import connectDB from './config/db.js'
import dotenv from 'dotenv'
import indexRouter from './src/routes/indexRouter.js'

//init app
const app = express()

//middleware
app.use(express.json())
dotenv.config()
connectDB()

//routes

app.use('/api', indexRouter)

const PORT = process.env.PORT || 3000

//server started
app.listen(PORT, () => {
  console.log(`Server started at PORT ${PORT}`)
})
